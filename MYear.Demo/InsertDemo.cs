﻿using MYear.ODA;
using MYear.ODA.Cmd;
using MYear.ODA.Ctx;
using MYear.ODA.Model;
using System;
using System.Data;

namespace MYear.Demo
{
    public class InsertDemo
    {
        [Demo(Demo = FuncType.Insert, MethodName = "Insert", MethodDescript = "插入指定字段的数据")]
        public static void Insert()
        {
            ODAContext ctx = BizContext.GetContext();
            var U = ctx.GetCmd<SysUser>();

            U.Insert(U.STATUS == "O", U.CREATED_BY == "Insert", U.LAST_UPDATED_BY == "User1", U.LAST_UPDATED_DATE == DateTime.Now, U.CREATED_DATE == DateTime.Now, U.EMAIL_ADDR == "riwfnsse@163.com",
                U.USER_ACCOUNT == "MYEAR_Insert", U.USER_NAME == "多年", U.USER_PASSWORD == "123", U.FE_MALE == "M", U.FAIL_TIMES == 0, U.IS_LOCKED == "N");

            ctx.From<SysRole>().Insert(r => new[] { r.ROLE_CODE == "Role", r.ROLE_NAME == "系统角色", r.ROLE_DESC, r.CREATED_BY == "Insert", r.LAST_UPDATED_BY == "Insert", r.STATUS == "O", r.LAST_UPDATED_DATE == DateTime.Now, r.CREATED_DATE == DateTime.Now });

            ctx.From<SysRole>().Insert(r => new[] { r.ROLE_CODE == "Admin", r.ROLE_NAME == "管理员", r.ROLE_DESC, r.CREATED_BY == "InsertModel", r.STATUS == "O", r.LAST_UPDATED_BY == "InsertModel", r.LAST_UPDATED_DATE == DateTime.Now, r.CREATED_DATE == DateTime.Now });


            ctx.From<SysRole>().Insert(r => new[] { r.ROLE_CODE == "Administrator", r.ROLE_NAME == "系统管理员", r.ROLE_DESC== "系统管理员",r.STATUS=="O", r.CREATED_BY == "InsertModel", r.LAST_UPDATED_BY == "InsertModel", r.LAST_UPDATED_DATE == DateTime.Now, r.CREATED_DATE == DateTime.Now });

            ctx.SYS_USER_ROLE().Insert(
                    new SYS_USER_ROLE
                    {
                        ROLE_CODE = "Role",
                        USER_ACCOUNT = "Admin",
                        CREATED_BY = "InsertModel",
                        CREATED_DATE = DateTime.Now,
                        STATUS = "O",
                        LAST_UPDATED_BY = "InsertModel",
                        LAST_UPDATED_DATE = DateTime.Now
                    }
                    );

            ctx.From<SysUserRole>().Insert(ur => new[] {  ur.USER_ACCOUNT == "MYEAR_Insert", ur.ROLE_CODE == "Admin", ur.STATUS == "O", ur.CREATED_BY == "Insert", ur.LAST_UPDATED_BY == "InsertModel", ur.LAST_UPDATED_DATE == DateTime.Now, ur.CREATED_DATE == DateTime.Now });
            ctx.From<SysUserRole>().Insert(ur => new[] { ur.USER_ACCOUNT == "MYEAR_Insert", ur.ROLE_CODE == "Administrator", ur.STATUS == "O", ur.CREATED_BY == "Insert", ur.LAST_UPDATED_BY == "InsertModel", ur.LAST_UPDATED_DATE == DateTime.Now, ur.CREATED_DATE == DateTime.Now });
            ctx.From<SysUserRole>().Insert(ur => new[] { ur.USER_ACCOUNT == "MYEAR_Model", ur.ROLE_CODE == "Admin", ur.STATUS == "O", ur.CREATED_BY == "Insert", ur.LAST_UPDATED_BY == "InsertModel", ur.LAST_UPDATED_DATE == DateTime.Now, ur.CREATED_DATE == DateTime.Now });
            ctx.From<SysUserRole>().Insert(ur => new[] { ur.USER_ACCOUNT == "MYEAR_Model", ur.ROLE_CODE == "Administrator", ur.STATUS == "O", ur.CREATED_BY == "Insert", ur.LAST_UPDATED_BY == "InsertModel", ur.LAST_UPDATED_DATE == DateTime.Now, ur.CREATED_DATE == DateTime.Now });

        }



        [Demo(Demo = FuncType.Insert, MethodName = "InsertModel", MethodDescript = "插入模型的数据")]
        public static void InsertModel()
        {
            ODAContext ctx = BizContext.GetContext();
            var U = ctx.GetCmd<SysUser>();
            U.Insert(new SYS_USER()
            {
                ADDRESS = "自由国度",
                CREATED_BY = "InsertModel",
                CREATED_DATE = DateTime.Now,
                FAIL_TIMES = 0,
                STATUS = "O",
                USER_ACCOUNT = "MYEAR_Model1122",
                USER_NAME = "多年1",
                USER_PASSWORD = "123",
                IS_LOCKED = "N",
                EMAIL_ADDR = "riwfnsse@163.com"
            });



            for (int i = 0; i < 100; i++)
            {
                string id = Guid.NewGuid().ToString("N");
                ctx.SYS_RESOURCE().Insert(
                    new SYS_RESOURCE()
                    {
                        ID = id,
                        RESOURCE_NAME = "RESOURCE_" + i.ToString(),
                        RESOURCE_INDEX = i,
                        STATUS = "O",
                        RESOURCE_DESC = "测试资源",
                        CREATED_BY = "InsertModel",
                        CREATED_DATE = DateTime.Now,
                        LAST_UPDATED_BY = "InsertModel",
                        LAST_UPDATED_DATE = DateTime.Now,
                        RESOURCE_LOCATION = "https://github.com/riwfnsse/MYear.ODA",
                        RESOURCE_SCOPE = "GENERAL" + id


                    });

                ctx.SYS_ROLE_AUTHORIZATION().Insert(

                    new SYS_ROLE_AUTHORIZATION()
                    {
                        RESOURCE_ID = id,
                        ROLE_CODE = "MYEAR_Insert",
                        IS_FORBIDDEN = i % 2 == 0 ? "Y" : "N",
                        CREATED_BY = "InsertModel",
                        CREATED_DATE = DateTime.Now,
                        LAST_UPDATED_BY = "InsertModel",
                        LAST_UPDATED_DATE = DateTime.Now,
                        STATUS = "O"

                    }

                    );

            }
        }

        [Demo(Demo = FuncType.Insert, MethodName = "Import", MethodDescript = "大批量导入数据")]
        public static string Import()
        {
            DateTime prepare = DateTime.Now;
            DataTable data = new DataTable();

            data.Columns.Add(new DataColumn("ADDRESS"));
            data.Columns.Add(new DataColumn("CREATED_BY"));
            data.Columns.Add(new DataColumn("CREATED_DATE",typeof(DateTime)));
            data.Columns.Add(new DataColumn("EMAIL_ADDR"));
            data.Columns.Add(new DataColumn("LAST_UPDATED_BY"));
            data.Columns.Add(new DataColumn("LAST_UPDATED_DATE", typeof(DateTime))); 
            data.Columns.Add(new DataColumn("FAIL_TIMES", typeof(decimal)));
            data.Columns.Add(new DataColumn("STATUS"));
            data.Columns.Add(new DataColumn("DUMMY"));
            data.Columns.Add(new DataColumn("USER_ACCOUNT"));
            data.Columns.Add(new DataColumn("USER_NAME"));
            data.Columns.Add(new DataColumn("USER_PASSWORD"));
            data.Columns.Add(new DataColumn("IS_LOCKED"));
             
            for (int i = 0; i < 10000; i++)
            {
                object[] dr = new object[]
                {
                    "第二次批量导入",
                    "Import" + i.ToString() ,
                    DateTime.Now,
                    "riwfnsse@163.com",
                    "User1" ,
                    DateTime.Now,
                    0,
                    "O",
                     "Dummy",
                    "User" + DateTime.Now.GetHashCode().ToString() + i.ToString(),
                    "导入的用户" + i.ToString(),
                    "123",
                    "N"                   
                };
                data.Rows.Add(dr); 
            }
            ODAContext ctx = BizContext.GetContext();
            var U = ctx.GetCmd<SysUser>();

            DateTime Begin = DateTime.Now;
            U.Import(data);
            DateTime end1 = DateTime.Now;
            return "导入完成,prepare " + prepare.ToString("yyyy-MM-dd HH:mm:ss.fffff") + " begin " + Begin.ToString("yyyy-MM-dd HH:mm:ss.fffff") + " end " + end1.ToString("yyyy-MM-dd HH:mm:ss.fffff");
        }


        [Demo(Demo = FuncType.Insert, MethodName = "Lambda", MethodDescript = "插入模型的数据")]
        public static void Lambda()
        {
            ODAContext ctx = BizContext.GetContext();
            var U = ctx.From<SysUser>().Insert(a => new[]{a.ADDRESS == "自由国度",a.CREATED_BY =="Lambda", a.USER_NAME=="Lambda",a.CREATED_DATE == DateTime.Now, a.EMAIL_ADDR=="riwfnsse@163.com",
                a.FAIL_TIMES == 0,a.STATUS =="O",a.USER_ACCOUNT=="MYear1" + Guid.NewGuid().ToString("N"),a.USER_PASSWORD=="123",a.IS_LOCKED =="N"}
                );
        }
    }
}
