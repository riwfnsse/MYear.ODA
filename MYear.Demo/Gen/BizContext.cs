﻿using MYear.ODA;
using MYear.ODA.Cmd;

namespace MYear.ODA.Ctx
{
	internal static class BizContext
	{
		internal static ODAContext GetContext()
		{
			//return new ODAContext(DbAType.MsSQL, "server=JCD202006003152;database=myear;uid=sa;pwd=123;");
			
			//单个库全局设定 ODAContext.SetODAConfig(ODA.DbAType.DB2, this.cbbxConnectstring.Text);
			return new ODAContext();
		}
        internal static SysDataDictionary SYS_DATA_DICTIONARY(this ODAContext Ctx)
        {
            return Ctx.GetCmd<SysDataDictionary>();
        }
        //internal static JoinCmd<SysDataDictionary> SYS_DATA_DICTIONARY(this ODAContext Ctx)
        //{
        //    return Ctx.GetJoinCmd<SysDataDictionary>();
        //}
        internal static SysResource SYS_RESOURCE(this ODAContext Ctx)
        {
            return Ctx.GetCmd<SysResource>();
        }
        //internal static JoinCmd<SysResource> SYS_RESOURCE(this ODAContext Ctx)
        //{
        //    return Ctx.GetJoinCmd<SysResource>();
        //}
        internal static SysRole SYS_ROLE(this ODAContext Ctx)
        {
            return Ctx.GetCmd<SysRole>();
        }
        //internal static JoinCmd<SysRole> SYS_ROLE(this ODAContext Ctx)
        //{
        //    return Ctx.GetJoinCmd<SysRole>();
        //}
        internal static SysRoleAuthorization SYS_ROLE_AUTHORIZATION(this ODAContext Ctx)
        {
            return Ctx.GetCmd<SysRoleAuthorization>();
        }
        //internal static JoinCmd<SysRoleAuthorization> SYS_ROLE_AUTHORIZATION(this ODAContext Ctx)
        //{
        //    return Ctx.GetJoinCmd<SysRoleAuthorization>();
        //}
        internal static SysUser SYS_USER(this ODAContext Ctx)
        {
            return Ctx.GetCmd<SysUser>();
        }
        //internal static JoinCmd<SysUser> SYS_USER(this ODAContext Ctx)
        //{
        //    return Ctx.GetJoinCmd<SysUser>();
        //}
        internal static SysUserAuthorization SYS_USER_AUTHORIZATION(this ODAContext Ctx)
        {
            return Ctx.GetCmd<SysUserAuthorization>();
        }
        //internal static JoinCmd<SysUserAuthorization> SYS_USER_AUTHORIZATION(this ODAContext Ctx)
        //{
        //    return Ctx.GetJoinCmd<SysUserAuthorization>();
        //}
        internal static SysUserRole SYS_USER_ROLE(this ODAContext Ctx)
        {
            return Ctx.GetCmd<SysUserRole>();
        }
        //internal static JoinCmd<SysUserRole> SYS_USER_ROLE(this ODAContext Ctx)
        //{
        //    return Ctx.GetJoinCmd<SysUserRole>();
        //}
        internal static VSysUserResource V_SYS_USER_RESOURCE(this ODAContext Ctx)
        {
            return Ctx.GetCmd<VSysUserResource>();
        }
        //internal static JoinCmd<VSysUserResource> V_SYS_USER_RESOURCE(this ODAContext Ctx)
        //{
        //    return Ctx.GetJoinCmd<VSysUserResource>();
        //}
    }
}
