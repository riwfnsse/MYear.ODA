using System;
using System.Data;
using System.Collections.Generic;
using System.Reflection;
using MYear.ODA;
using MYear.ODA.Model;

namespace MYear.ODA.Cmd
{
	internal partial class SysDataDictionary : ORMCmd<SYS_DATA_DICTIONARY>
	{
		public ODAColumns STATUS { get { return new ODAColumns(this, "STATUS", ODAdbType.OChar, 1, true); } }
		public ODAColumns CREATED_BY { get { return new ODAColumns(this, "CREATED_BY", ODAdbType.OVarchar, 80, false); } }
		public ODAColumns CREATED_DATE { get { return new ODAColumns(this, "CREATED_DATE", ODAdbType.ODatetime, 8, false); } }
		public ODAColumns LAST_UPDATED_BY { get { return new ODAColumns(this, "LAST_UPDATED_BY", ODAdbType.OVarchar, 80, false); } }
		public ODAColumns LAST_UPDATED_DATE { get { return new ODAColumns(this, "LAST_UPDATED_DATE", ODAdbType.ODatetime, 8, false); } }
		public ODAColumns TABLE_CODE { get { return new ODAColumns(this, "TABLE_CODE", ODAdbType.OChar, 36, true); } }
		public ODAColumns COLUMN_CODE { get { return new ODAColumns(this, "COLUMN_CODE", ODAdbType.OChar, 36, true); } }
		public ODAColumns OBJ_NAME { get { return new ODAColumns(this, "OBJ_NAME", ODAdbType.OVarchar, 80, false); } }
		public ODAColumns OBJ_DESC { get { return new ODAColumns(this, "OBJ_DESC", ODAdbType.OVarchar, 2000, false); } }
		public ODAColumns ZHCN_NAME { get { return new ODAColumns(this, "ZHCN_NAME", ODAdbType.OVarchar, 80, false); } }
		public ODAColumns ENUS_NAME { get { return new ODAColumns(this, "ENUS_NAME", ODAdbType.OVarchar, 80, false); } }
		public override string CmdName { get { return "SYS_DATA_DICTIONARY"; } }
		public override List<ODAColumns> GetColumnList()
		{
			return new List<ODAColumns>() { STATUS, CREATED_BY, CREATED_DATE, LAST_UPDATED_BY, LAST_UPDATED_DATE, TABLE_CODE, COLUMN_CODE, OBJ_NAME, OBJ_DESC, ZHCN_NAME, ENUS_NAME };
		}
	}
	internal partial class SysResource : ORMCmd<SYS_RESOURCE>
	{
		public ODAColumns STATUS { get { return new ODAColumns(this, "STATUS", ODAdbType.OChar, 1, true); } }
		public ODAColumns CREATED_BY { get { return new ODAColumns(this, "CREATED_BY", ODAdbType.OVarchar, 80, false); } }
		public ODAColumns CREATED_DATE { get { return new ODAColumns(this, "CREATED_DATE", ODAdbType.ODatetime, 8, false); } }
		public ODAColumns LAST_UPDATED_BY { get { return new ODAColumns(this, "LAST_UPDATED_BY", ODAdbType.OVarchar, 80, false); } }
		public ODAColumns LAST_UPDATED_DATE { get { return new ODAColumns(this, "LAST_UPDATED_DATE", ODAdbType.ODatetime, 8, false); } }
		public ODAColumns ID { get { return new ODAColumns(this, "ID", ODAdbType.OChar, 36, true); } }
		public ODAColumns RESOURCE_NAME { get { return new ODAColumns(this, "RESOURCE_NAME", ODAdbType.OVarchar, 80, false); } }
		public ODAColumns RESOURCE_DESC { get { return new ODAColumns(this, "RESOURCE_DESC", ODAdbType.OVarchar, 2000, false); } }
		public ODAColumns RESOURCE_TYPE { get { return new ODAColumns(this, "RESOURCE_TYPE", ODAdbType.OChar, 36, true); } }
		public ODAColumns RESOURCE_SCOPE { get { return new ODAColumns(this, "RESOURCE_SCOPE", ODAdbType.OChar, 36, true); } }
		public ODAColumns RESOURCE_LOCATION { get { return new ODAColumns(this, "RESOURCE_LOCATION", ODAdbType.OVarchar, 600, true); } }
		public ODAColumns PARENT_ID { get { return new ODAColumns(this, "PARENT_ID", ODAdbType.OVarchar, 36, false); } }
		public ODAColumns PARENT_ID1 { get { return new ODAColumns(this, "PARENT_ID1", ODAdbType.OVarchar, 36, false); } }
		public ODAColumns PARENT_ID2 { get { return new ODAColumns(this, "PARENT_ID2", ODAdbType.OVarchar, 36, false); } }
		public ODAColumns PARENT_ID3 { get { return new ODAColumns(this, "PARENT_ID3", ODAdbType.OVarchar, 36, false); } }
		public ODAColumns PARENT_ID4 { get { return new ODAColumns(this, "PARENT_ID4", ODAdbType.OVarchar, 36, false); } }
		public ODAColumns PARENT_ID5 { get { return new ODAColumns(this, "PARENT_ID5", ODAdbType.OVarchar, 36, false); } }
		public ODAColumns PARENT_ID6 { get { return new ODAColumns(this, "PARENT_ID6", ODAdbType.OVarchar, 36, false); } }
		public ODAColumns RESOURCE_INDEX { get { return new ODAColumns(this, "RESOURCE_INDEX", ODAdbType.OInt, 4, false); } }
		public override string CmdName { get { return "SYS_RESOURCE"; } }
		public override List<ODAColumns> GetColumnList()
		{
			return new List<ODAColumns>() { STATUS, CREATED_BY, CREATED_DATE, LAST_UPDATED_BY, LAST_UPDATED_DATE, ID, RESOURCE_NAME, RESOURCE_DESC, RESOURCE_TYPE, RESOURCE_SCOPE, RESOURCE_LOCATION, PARENT_ID, PARENT_ID1, PARENT_ID2, PARENT_ID3, PARENT_ID4, PARENT_ID5, PARENT_ID6, RESOURCE_INDEX };
		}
	}
	internal partial class SysRole : ORMCmd<SYS_ROLE>
	{
		public ODAColumns STATUS { get { return new ODAColumns(this, "STATUS", ODAdbType.OChar, 1, true); } }
		public ODAColumns CREATED_BY { get { return new ODAColumns(this, "CREATED_BY", ODAdbType.OVarchar, 80, false); } }
		public ODAColumns CREATED_DATE { get { return new ODAColumns(this, "CREATED_DATE", ODAdbType.ODatetime, 8, false); } }
		public ODAColumns LAST_UPDATED_BY { get { return new ODAColumns(this, "LAST_UPDATED_BY", ODAdbType.OVarchar, 80, false); } }
		public ODAColumns LAST_UPDATED_DATE { get { return new ODAColumns(this, "LAST_UPDATED_DATE", ODAdbType.ODatetime, 8, false); } }
		public ODAColumns ROLE_CODE { get { return new ODAColumns(this, "ROLE_CODE", ODAdbType.OChar, 36, true); } }
		public ODAColumns ROLE_NAME { get { return new ODAColumns(this, "ROLE_NAME", ODAdbType.OVarchar, 80, false); } }
		public ODAColumns ROLE_DESC { get { return new ODAColumns(this, "ROLE_DESC", ODAdbType.OVarchar, 2000, false); } }
		public override string CmdName { get { return "SYS_ROLE"; } }
		public override List<ODAColumns> GetColumnList()
		{
			return new List<ODAColumns>() { STATUS, CREATED_BY, CREATED_DATE, LAST_UPDATED_BY, LAST_UPDATED_DATE, ROLE_CODE, ROLE_NAME, ROLE_DESC };
		}
	}
	internal partial class SysRoleAuthorization : ORMCmd<SYS_ROLE_AUTHORIZATION>
	{
		public ODAColumns STATUS { get { return new ODAColumns(this, "STATUS", ODAdbType.OChar, 1, true); } }
		public ODAColumns CREATED_BY { get { return new ODAColumns(this, "CREATED_BY", ODAdbType.OVarchar, 80, false); } }
		public ODAColumns CREATED_DATE { get { return new ODAColumns(this, "CREATED_DATE", ODAdbType.ODatetime, 8, false); } }
		public ODAColumns LAST_UPDATED_BY { get { return new ODAColumns(this, "LAST_UPDATED_BY", ODAdbType.OVarchar, 80, false); } }
		public ODAColumns LAST_UPDATED_DATE { get { return new ODAColumns(this, "LAST_UPDATED_DATE", ODAdbType.ODatetime, 8, false); } }
		public ODAColumns ROLE_CODE { get { return new ODAColumns(this, "ROLE_CODE", ODAdbType.OChar, 36, true); } }
		public ODAColumns RESOURCE_ID { get { return new ODAColumns(this, "RESOURCE_ID", ODAdbType.OChar, 36, true); } }
		public ODAColumns IS_FORBIDDEN { get { return new ODAColumns(this, "IS_FORBIDDEN", ODAdbType.OChar, 1, true); } }
		public override string CmdName { get { return "SYS_ROLE_AUTHORIZATION"; } }
		public override List<ODAColumns> GetColumnList()
		{
			return new List<ODAColumns>() { STATUS, CREATED_BY, CREATED_DATE, LAST_UPDATED_BY, LAST_UPDATED_DATE, ROLE_CODE, RESOURCE_ID, IS_FORBIDDEN };
		}
	}
	internal partial class SysUser : ORMCmd<SYS_USER>
	{
		public ODAColumns STATUS { get { return new ODAColumns(this, "STATUS", ODAdbType.OChar, 1, true); } }
		public ODAColumns CREATED_BY { get { return new ODAColumns(this, "CREATED_BY", ODAdbType.OVarchar, 80, false); } }
		public ODAColumns CREATED_DATE { get { return new ODAColumns(this, "CREATED_DATE", ODAdbType.ODatetime, 8, false); } }
		public ODAColumns LAST_UPDATED_BY { get { return new ODAColumns(this, "LAST_UPDATED_BY", ODAdbType.OVarchar, 80, false); } }
		public ODAColumns LAST_UPDATED_DATE { get { return new ODAColumns(this, "LAST_UPDATED_DATE", ODAdbType.ODatetime, 8, false); } }
		public ODAColumns USER_ACCOUNT { get { return new ODAColumns(this, "USER_ACCOUNT", ODAdbType.OChar, 36, true); } }
		public ODAColumns USER_NAME { get { return new ODAColumns(this, "USER_NAME", ODAdbType.OVarchar, 80, true); } }
		public ODAColumns USER_PASSWORD { get { return new ODAColumns(this, "USER_PASSWORD", ODAdbType.OVarchar, 200, true); } }
		public ODAColumns EMAIL_ADDR { get { return new ODAColumns(this, "EMAIL_ADDR", ODAdbType.OVarchar, 200, false); } }
		public ODAColumns PHONE_NO { get { return new ODAColumns(this, "PHONE_NO", ODAdbType.OVarchar, 80, false); } }
		public ODAColumns ADDRESS { get { return new ODAColumns(this, "ADDRESS", ODAdbType.OVarchar, 200, false); } }
		public ODAColumns FE_MALE { get { return new ODAColumns(this, "FE_MALE", ODAdbType.OChar, 1, false); } }
		public ODAColumns FAIL_TIMES { get { return new ODAColumns(this, "FAIL_TIMES", ODAdbType.OInt, 4, true); } }
		public ODAColumns IS_LOCKED { get { return new ODAColumns(this, "IS_LOCKED", ODAdbType.OChar, 1, true); } }
		public override string CmdName { get { return "SYS_USER"; } }
		public override List<ODAColumns> GetColumnList()
		{
			return new List<ODAColumns>() { STATUS, CREATED_BY, CREATED_DATE, LAST_UPDATED_BY, LAST_UPDATED_DATE, USER_ACCOUNT, USER_NAME, USER_PASSWORD, EMAIL_ADDR, PHONE_NO, ADDRESS, FE_MALE, FAIL_TIMES, IS_LOCKED };
		}
	}
	internal partial class SysUserAuthorization : ORMCmd<SYS_USER_AUTHORIZATION>
	{
		public ODAColumns STATUS { get { return new ODAColumns(this, "STATUS", ODAdbType.OChar, 1, true); } }
		public ODAColumns CREATED_BY { get { return new ODAColumns(this, "CREATED_BY", ODAdbType.OVarchar, 80, false); } }
		public ODAColumns CREATED_DATE { get { return new ODAColumns(this, "CREATED_DATE", ODAdbType.ODatetime, 8, false); } }
		public ODAColumns LAST_UPDATED_BY { get { return new ODAColumns(this, "LAST_UPDATED_BY", ODAdbType.OVarchar, 80, false); } }
		public ODAColumns LAST_UPDATED_DATE { get { return new ODAColumns(this, "LAST_UPDATED_DATE", ODAdbType.ODatetime, 8, false); } }
		public ODAColumns USER_ACCOUNT { get { return new ODAColumns(this, "USER_ACCOUNT", ODAdbType.OChar, 36, true); } }
		public ODAColumns RESOURCE_ID { get { return new ODAColumns(this, "RESOURCE_ID", ODAdbType.OChar, 36, true); } }
		public ODAColumns IS_FORBIDDEN { get { return new ODAColumns(this, "IS_FORBIDDEN", ODAdbType.OChar, 1, true); } }
		public override string CmdName { get { return "SYS_USER_AUTHORIZATION"; } }
		public override List<ODAColumns> GetColumnList()
		{
			return new List<ODAColumns>() { STATUS, CREATED_BY, CREATED_DATE, LAST_UPDATED_BY, LAST_UPDATED_DATE, USER_ACCOUNT, RESOURCE_ID, IS_FORBIDDEN };
		}
	}
	internal partial class SysUserRole : ORMCmd<SYS_USER_ROLE>
	{
		public ODAColumns STATUS { get { return new ODAColumns(this, "STATUS", ODAdbType.OChar, 1, true); } }
		public ODAColumns CREATED_BY { get { return new ODAColumns(this, "CREATED_BY", ODAdbType.OVarchar, 80, false); } }
		public ODAColumns CREATED_DATE { get { return new ODAColumns(this, "CREATED_DATE", ODAdbType.ODatetime, 8, false); } }
		public ODAColumns LAST_UPDATED_BY { get { return new ODAColumns(this, "LAST_UPDATED_BY", ODAdbType.OVarchar, 80, false); } }
		public ODAColumns LAST_UPDATED_DATE { get { return new ODAColumns(this, "LAST_UPDATED_DATE", ODAdbType.ODatetime, 8, false); } }
		public ODAColumns USER_ACCOUNT { get { return new ODAColumns(this, "USER_ACCOUNT", ODAdbType.OChar, 36, true); } }
		public ODAColumns ROLE_CODE { get { return new ODAColumns(this, "ROLE_CODE", ODAdbType.OChar, 36, true); } }
		public override string CmdName { get { return "SYS_USER_ROLE"; } }
		public override List<ODAColumns> GetColumnList()
		{
			return new List<ODAColumns>() { STATUS, CREATED_BY, CREATED_DATE, LAST_UPDATED_BY, LAST_UPDATED_DATE, USER_ACCOUNT, ROLE_CODE };
		}
	}
	internal partial class VSysUserResource : ORMCmd<V_SYS_USER_RESOURCE>
	{
		public override bool Insert(params IODAColumns[] Cols) { throw new ODAException("Not suport Insert CmdName " + CmdName); }
		public override bool Update(params IODAColumns[] Cols) { throw new ODAException("Not Suport Update CmdName " + CmdName); }
		public override bool Delete() { throw new ODAException("Not Suport Delete CmdName " + CmdName); }
		public ODAColumns IS_FORBIDDEN { get { return new ODAColumns(this, "IS_FORBIDDEN", ODAdbType.OChar, 1, true); } }
		public ODAColumns RESOURCE_ID { get { return new ODAColumns(this, "RESOURCE_ID", ODAdbType.OChar, 36, true); } }
		public ODAColumns RESOURCE_LOCATION { get { return new ODAColumns(this, "RESOURCE_LOCATION", ODAdbType.OVarchar, 600, true); } }
		public ODAColumns RESOURCE_NAME { get { return new ODAColumns(this, "RESOURCE_NAME", ODAdbType.OVarchar, 80, false); } }
		public ODAColumns RESOURCE_SCOPE { get { return new ODAColumns(this, "RESOURCE_SCOPE", ODAdbType.OChar, 36, true); } }
		public ODAColumns RESOURCE_TYPE { get { return new ODAColumns(this, "RESOURCE_TYPE", ODAdbType.OChar, 36, true); } }
		public ODAColumns USER_ACCOUNT { get { return new ODAColumns(this, "USER_ACCOUNT", ODAdbType.OChar, 36, true); } }
		public ODAColumns USER_NAME { get { return new ODAColumns(this, "USER_NAME", ODAdbType.OVarchar, 80, true); } }
		public override string CmdName { get { return "V_SYS_USER_RESOURCE"; } }
		public override List<ODAColumns> GetColumnList()
		{
			return new List<ODAColumns>() { IS_FORBIDDEN, RESOURCE_ID, RESOURCE_LOCATION, RESOURCE_NAME, RESOURCE_SCOPE, RESOURCE_TYPE, USER_ACCOUNT, USER_NAME };
		}
	}
}
