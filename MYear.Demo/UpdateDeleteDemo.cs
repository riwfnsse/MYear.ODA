﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MYear.ODA.Cmd;
using MYear.ODA.Model;
using MYear.ODA;
using System.Data;
using System;
using MYear.ODA.Ctx;
using System.Drawing.Printing;

namespace MYear.Demo
{
    public class UpdateDeleteDemo
    {
        [Demo(Demo = FuncType.UpdateDelete, MethodName = "Update", MethodDescript = "更新数据")]
        public static void Update()
        {
            ///Update 的 where 条件可参考 SELECT 语句 
            ODAContext ctx = BizContext.GetContext();
            var U = ctx.SYS_USER();
            U.Where(U.LAST_UPDATED_BY == "InsertModel", U.IS_LOCKED == "N", U.STATUS == "O", U.EMAIL_ADDR.IsNotNull)
             .Update(
                U.USER_NAME == "新的名字", U.IS_LOCKED == "Y"
                );
        }
        [Demo(Demo = FuncType.UpdateDelete, MethodName = "UpdateModel", MethodDescript = "模型数据Upadte")]
        public static void UpdateModel()
        {
            ///使用实体 Update 数据时，对于属性值为 null 的字段不作更新。
            ///这是由于在ORM组件的实际应用中，多数时候界面回传的是完整的实体对象，
            ///或者收接时使用完整的实体作为反序列化的容器，那些不需要更新的字段也在其中，而且为null.
            ODAContext ctx = BizContext.GetContext();
            var U = ctx.SYS_USER();
            U.Where(U.CREATED_BY == "InsertModel", U.IS_LOCKED == "N", U.STATUS == "O", U.EMAIL_ADDR.IsNotNull)
             .Update(new SYS_USER()
            {
                ADDRESS = "自由国度", 
                CREATED_DATE = DateTime.Now, 
                STATUS = "O",
                USER_ACCOUNT = "NYear1",
                USER_NAME = "多年1",
                USER_PASSWORD = "123",
                IS_LOCKED = "N",
            });

        }

        [Demo(Demo = FuncType.UpdateDelete, MethodName = "UpdateCompute", MethodDescript = "更新运算")]
        public static void UpdateCompute()
        {
            ////支持的运算符号：+ 、 - 、*、/、%
            ///目前对一个字段更新时，只支持一个运算符号；
            ODAContext ctx = BizContext.GetContext();
            var U = ctx.SYS_USER();
            var data = U.Where(U.CREATED_BY == "Lambda", U.IS_LOCKED == "N", U.EMAIL_ADDR.IsNotNull)
                .Update(U.FAIL_TIMES == U.FAIL_TIMES + 1, U.USER_NAME == U.USER_ACCOUNT + U.EMAIL_ADDR ); 
        }

        [Demo(Demo = FuncType.UpdateDelete, MethodName = "Delete", MethodDescript = "删除数据")]
        public static void Delete()
        {
            ////Delete的where条件可参考 SELECT 语句 
            ODAContext ctx = new ODAContext();
            var U = ctx.GetCmd<SysUser>();
            var data = U.Where(U.USER_ACCOUNT.ContainLeft ( "User1"), U.IS_LOCKED == "N", U.EMAIL_ADDR.IsNotNull)
                .Delete();
        }


        [Demo(Demo = FuncType.UpdateDelete, MethodName = "DeleteLambda", MethodDescript = "删除数据")]
        public static void DeleteLambda()
        {
            

            ////Delete的where条件可参考 SELECT 语句 
            var data = BizContext.GetContext().From<SysUser>().Where(w=>w.CREATED_BY.ContainLeft("User2") & w.IS_LOCKED == "N" & w.EMAIL_ADDR.IsNotNull)
                .Delete();
        }



        [Demo(Demo = FuncType.UpdateDelete, MethodName = "UpdateLambda", MethodDescript = "更新")]
        public static void UpdateLambda()
        {
            var data = BizContext.GetContext().From<SysUser>().Where(w => w.CREATED_BY == "Lambda" & w.IS_LOCKED == "N" & w.EMAIL_ADDR.IsNotNull)
             .Update(v => new[] { v.FAIL_TIMES == v.FAIL_TIMES + 1, v.ADDRESS == v.USER_ACCOUNT + v.EMAIL_ADDR,v.USER_NAME == "Lambda更新" });
        }
    }
}