﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MYear.ODA
{
    public class ODACaseWhen
    {
        private List<object> _WhenThen = new List<object>();
        private IODACmd _Cmd;
        private object _Else = null;
        public ODACaseWhen(IODACmd Cmd)
        {
            _Cmd = Cmd;
        }
        public ODACaseWhen When(ODAColumns Col)
        {
            _WhenThen.Add(Col);
            return this;
        }

        public ODACaseWhen Then(object obj)
        {
            _WhenThen.Add(obj);
            return this;
        }
        public ODACaseWhen Else(object obj)
        {
            _Else = obj;
            return this;
        }
        public ODAColumns End()
        {
            return new ODAFunction(_Cmd).CaseWhen(_Else,_WhenThen.ToArray());
        }
    }
    public class ODACase 
    {
        private ODAFunction _Func;
        private ODAColumns _Col;
        private List<object> ParamList = new List<object>();
        private object _Else = null;
        public ODACase(ODAColumns Col)
        {
            _Col = Col; 
            _Func = new ODAFunction(((IODAColumns)Col).Cmd);
        }
        public ODACase When(object obj)
        {
            ParamList.Add(obj);
            return this;
        }

        public ODACase Then(object obj)
        {
            ParamList.Add(obj);
            return this;
        }
        public ODACase Esle(object obj)
        {
            _Else = obj;
            return this;
        }
        public ODAColumns End()
        {
            return _Func.Case(_Col, _Else, ParamList.ToArray());
        }

    }
}
