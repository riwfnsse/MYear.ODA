﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text; 

namespace MYear.ODA
{
    public static class ODAColumnExt
    {
        public static ODAColumns Decode(this ODAColumns Col,object DefaultVal,params object[] KeyValue)
        {
            var dc =  new ODAFunction( ((IODAColumns)Col).Cmd); 
            return dc.Decode(Col, DefaultVal, KeyValue); 
        }
        public static ODACase Case(this ODAColumns Col)
        {
            var cs = new ODACase(Col);
            return cs;
        }
        public static ODAColumns NullDefault(this ODAColumns Col, object DefVal)
        {
            var func = new ODAFunction(((IODAColumns)Col).Cmd);
            return func.NullDefault(Col, DefVal);
        }
        public static ODACaseWhen CaseWhen(this IODACmd Cmd, ODAColumns Col)
        {
            var cs = new ODACaseWhen(Cmd).When(Col);
            return cs;
        }
    }
}
