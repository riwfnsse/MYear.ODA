﻿using MYear.ODA;
using MYear.ODA.Cmd;

namespace MYear.PerformanceTest.ORMTest
{
    public static class ODATest
    {

        public static void ReadData()
        {
            ODAContext ctx = DBConfig.GetODAContext();
            var t = ctx.GetCmd<CmdTest>();
            var list = t.SelectM();
        }
        public static void Paging()
        {
            ODAContext ctx = DBConfig.GetODAContext();
            int total = 0;
            var t = ctx.GetCmd<CmdTest>();
            var list = t.SelectM(20 * 50, 50, out total);
        }
        public static void Sql()
        {
            ODAContext ctx = DBConfig.GetODAContext();
            var U = ctx.GetCmd<SysUser>();
            var UR = ctx.GetCmd<SysUserRole>();

            var data = U.InnerJoin(UR, U.USER_ACCOUNT == UR.USER_ACCOUNT, UR.STATUS == "O")
              .Where(U.STATUS == "O", U.EMAIL_ADDR.IsNotNull.Or(U.EMAIL_ADDR == "riwfnsse@163.com"),
              U.IS_LOCKED == "N" ,
               UR.ROLE_CODE.In("Administrator", "Admin", "PowerUser", "User", "Guest"))
              .Groupby(UR.ROLE_CODE)
              .Having(U.USER_ACCOUNT.Count > 2)
              .OrderbyAsc(U.USER_ACCOUNT.Count)
              .Select(U.USER_ACCOUNT.Count.As("USER_COUNT"), UR.ROLE_CODE); 
        }
        public static void GetById()
        {
            ODAContext ctx = DBConfig.GetODAContext();
            var t = ctx.GetCmd<CmdTest>();
            var list = t.Where(t.ID == 1).SelectM();
        }
    }

}
