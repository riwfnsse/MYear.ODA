using System;
using System.Data;
using System.Collections.Generic;
using System.Reflection;
using MYear.ODA;
using MYear.ODA.Model;

namespace MYear.ODA.Cmd
{
	 
	internal partial class SysUser:ORMCmd<SYS_USER>
	{
		 public ODAColumns STATUS{ get { return new ODAColumns(this, "STATUS", ODAdbType.OChar, 1,true ); } }
		 public ODAColumns USER_NAME{ get { return new ODAColumns(this, "CREATED_BY", ODAdbType.OVarchar, 80,false ); } }
		 public ODAColumns CREATED_DATE{ get { return new ODAColumns(this, "CREATED_DATE", ODAdbType.ODatetime, 7,false ); } }
		 public ODAColumns LAST_UPDATED_BY{ get { return new ODAColumns(this, "LAST_UPDATED_BY", ODAdbType.OVarchar, 80,false ); } }
		 public ODAColumns LAST_UPDATED_DATE{ get { return new ODAColumns(this, "LAST_UPDATED_DATE", ODAdbType.ODatetime, 7,false ); } }
		 public ODAColumns USER_ACCOUNT{ get { return new ODAColumns(this, "USER_ACCOUNT", ODAdbType.OChar, 36,true ); } }
		 public ODAColumns USER_PASSWORD{ get { return new ODAColumns(this, "USER_PASSWORD", ODAdbType.OVarchar, 200,false ); } }
		 public ODAColumns EMAIL_ADDR{ get { return new ODAColumns(this, "EMAIL_ADDR", ODAdbType.OVarchar, 200,false ); } }
		 public ODAColumns PHONE_NO{ get { return new ODAColumns(this, "PHONE_NO", ODAdbType.OVarchar, 80,false ); } }
		 public ODAColumns ADDRESS{ get { return new ODAColumns(this, "ADDRESS", ODAdbType.OVarchar, 200,false ); } }
		 public ODAColumns FE_MALE{ get { return new ODAColumns(this, "FE_MALE", ODAdbType.OChar, 1,false ); } }
		 public ODAColumns FAIL_TIMES{ get { return new ODAColumns(this, "FAIL_TIMES", ODAdbType.ODecimal, 22,true ); } }
		 public ODAColumns IS_LOCKED{ get { return new ODAColumns(this, "IS_LOCKED", ODAdbType.OChar, 1,true ); } }
		 public override string CmdName { get { return "SYS_USER"; }}
		 public override List<ODAColumns> GetColumnList() 
		 { 
			 return new List<ODAColumns>() { STATUS,CREATED_DATE,LAST_UPDATED_BY,LAST_UPDATED_DATE,USER_ACCOUNT,USER_NAME,USER_PASSWORD,EMAIL_ADDR,PHONE_NO,ADDRESS,FE_MALE,FAIL_TIMES,IS_LOCKED};
		 }
	} 
	internal partial class SysUserRole:ORMCmd<SYS_USER_ROLE>
	{
		 public ODAColumns STATUS{ get { return new ODAColumns(this, "STATUS", ODAdbType.OChar, 1,true ); } }
		 public ODAColumns USER_NAME{ get { return new ODAColumns(this, "CREATED_BY", ODAdbType.OVarchar, 80,false ); } }
		 public ODAColumns CREATED_DATE{ get { return new ODAColumns(this, "CREATED_DATE", ODAdbType.ODatetime, 7,false ); } }
		 public ODAColumns LAST_UPDATED_BY{ get { return new ODAColumns(this, "LAST_UPDATED_BY", ODAdbType.OVarchar, 80,false ); } }
		 public ODAColumns LAST_UPDATED_DATE{ get { return new ODAColumns(this, "LAST_UPDATED_DATE", ODAdbType.ODatetime, 7,false ); } }
		 public ODAColumns USER_ACCOUNT{ get { return new ODAColumns(this, "USER_ACCOUNT", ODAdbType.OChar, 36,true ); } }
		 public ODAColumns ROLE_CODE{ get { return new ODAColumns(this, "ROLE_CODE", ODAdbType.OChar, 36,true ); } }
		 public override string CmdName { get { return "SYS_USER_ROLE"; }}
		 public override List<ODAColumns> GetColumnList() 
		 { 
			 return new List<ODAColumns>() { STATUS,USER_NAME,CREATED_DATE,LAST_UPDATED_BY,LAST_UPDATED_DATE,USER_ACCOUNT,ROLE_CODE};
		 }
	}

    internal partial class CmdTest : ORMCmd<Test>
    {
        public ODAColumns ID { get { return new ODAColumns(this, "Id", ODAdbType.OInt, 4, true); } }
        public ODAColumns ColFByte { get { return new ODAColumns(this, "F_Byte", ODAdbType.OInt, 1, false); } }
        public ODAColumns ColFInt16 { get { return new ODAColumns(this, "F_Int16", ODAdbType.OInt, 2, false); } }
        public ODAColumns ColFInt32 { get { return new ODAColumns(this, "F_Int32", ODAdbType.OInt, 4, false); } }
        public ODAColumns ColFInt64 { get { return new ODAColumns(this, "F_Int64", ODAdbType.ODecimal, 8, false); } }
        public ODAColumns ColFDouble { get { return new ODAColumns(this, "F_Double", ODAdbType.ODecimal, 8, false); } }
        public ODAColumns ColFFloat { get { return new ODAColumns(this, "F_Float", ODAdbType.ODecimal, 4, false); } }
        public ODAColumns ColFDecimal { get { return new ODAColumns(this, "F_Decimal", ODAdbType.ODecimal, 9, false); } }
        public ODAColumns ColFBool { get { return new ODAColumns(this, "F_Bool", ODAdbType.OInt, 1, false); } }
        public ODAColumns ColFDatetime { get { return new ODAColumns(this, "F_DateTime", ODAdbType.ODatetime, 8, false); } }
        public ODAColumns ColFGuid { get { return new ODAColumns(this, "F_Guid", ODAdbType.OVarchar, 16, false); } }
        public ODAColumns ColFString { get { return new ODAColumns(this, "F_String", ODAdbType.OVarchar, 200, false); } }
        //public ODAColumns ColFBytes { get { return new ODAColumns(this, "F_Bytes", ODAdbType.OBinary, 16, false); } }
        public override string CmdName { get { return "TEST"; } }
        public override List<ODAColumns> GetColumnList()
        {
            return new List<ODAColumns>() { ID, ColFByte, ColFInt16, ColFInt32, ColFInt64, ColFDouble, ColFFloat, ColFDecimal, ColFBool, ColFDatetime, ColFGuid, ColFString };
        }
    }
}
